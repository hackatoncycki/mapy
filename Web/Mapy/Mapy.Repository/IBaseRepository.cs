﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Mapy.Repository
{
    public interface IBaseRepository
    {
    }

    public interface IBaseRepository<TBase> where TBase : class
    {
        TBase GetEntity(int id);
        TBase GetEntityBy(Expression<Func<TBase, bool>> predicate);
        TBase GetEntityBy(Expression<Func<TBase, bool>> predicate, params Expression<Func<TBase, object>>[] includeProperties);
        TBase AddEntity(TBase entity);
        TBase UpdateEntity(TBase entity);
        void DeleteEntity(TBase entity);
        List<TBase> GetList();
        List<TBase> GetList(params Expression<Func<TBase, object>>[] includeProperties);
        List<TBase> GetListBy(Expression<Func<TBase, bool>> predicate);
        int GetCountListBy(Expression<Func<TBase, bool>> predicate);
        List<TBase> GetListBy(Expression<Func<TBase, bool>> predicate, params Expression<Func<TBase, object>>[] includeProperties);
        List<TBase> GetPaginatedListBy<TKey>(Expression<Func<TBase, bool>> whereLambda, Expression<Func<TBase, TKey>> orderByLambda, int skipCount, int takeCount);
        List<TBase> GetPaginatedListBy<TKey>(Expression<Func<TBase, bool>> whereLambda, Expression<Func<TBase, TKey>> orderByLambda, int skipCount, int takeCount, params Expression<Func<TBase, object>>[] includeProperties);
    }
}