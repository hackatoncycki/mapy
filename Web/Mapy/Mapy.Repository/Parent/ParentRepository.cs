﻿using Mapy.Model;
using Mapy.Model.Entities;
using Mapy.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mapy.Repository.Parent
{
    public class ParentRepository : BaseRepository<ApplicationUser>, IParentRepository
    {
    }
}
