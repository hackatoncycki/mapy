﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mapy.Repository.Target
{
    public interface ITargetRepository : IBaseRepository<Mapy.Model.Entities.Target>
    {
    }
}
