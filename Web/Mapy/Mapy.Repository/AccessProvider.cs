﻿using Mapy.Model;
using Mapy.Models;
using System;
using System.Web;

namespace Mapy.Repository
{
    public class AccessProvider
    {
        /// <summary>
        /// Nazwa klucza w kontekście zapytania HTTP
        /// </summary>
        private static readonly string m_CONTEXT_KEY = "ObjectContext";

        private static ApplicationDbContext APContext;

        public static void ClearContext()
        {
            HttpContext context = HttpContext.Current;
            if (context != null && context.Items[m_CONTEXT_KEY] != null)
            {
                context.Items[m_CONTEXT_KEY] = null;
            }
        }


        public static ApplicationDbContext GetObjectContext()
        {
            return GetObjectContext(true);
        }

        /// <summary>
        /// Metoda zwracająca kontekst danych PER kontekst zapytania HTTP
        /// </summary>
        /// <returns></returns>

        public static ApplicationDbContext GetObjectContext(bool useHttpContext)
        {
            if (useHttpContext)
            {
                //pobieramy kontekst aktualnego zapytania HTTP
                HttpContext context = HttpContext.Current;
                if (context == null)
                {
                    if (APContext == null)
                    {
                        APContext = new ApplicationDbContext();
                    }
                    else
                    {
                        try
                        {
                            APContext.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                            //Elmah.ErrorLog.GetDefault(HttpContext.Current).Log(new Elmah.Error(ex, HttpContext.Current));
                        }
                    }
                    return APContext;
                }

                //Jesli nie istnieje wpis o kluczu m_CONTEXT_KEY = "ObjectContext"
                if (context.Items[m_CONTEXT_KEY] == null)
                {
                    //To instancjalizujemy kontekst danych i ...
                    ApplicationDbContext entities = new ApplicationDbContext();
                    // dorzucamy go do kontekst zapytania HTTP
                    context.Items[m_CONTEXT_KEY] = entities;
                    //Zwracamy kontekst danych
                    return entities;
                }
                //A jeśli istnieje wpis o kluczu m_CONTEXT_KEY = "ObjectContext" w 
                //kontekście aktualnego zapytania HTTP
                //to zwracamy konteksta danych znajdujący sie pod tym wpisem
                return context.Items[m_CONTEXT_KEY] as ApplicationDbContext;
            }
            else
            {
                if (APContext != null)
                {
                    APContext.Dispose();
                    APContext = null;
                }

                APContext = new ApplicationDbContext();
                return APContext;
            }
        }
    }
}
