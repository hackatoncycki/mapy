﻿using Mapy.Model;
using Mapy.Models;
using Mapy.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Mapy.Repository
{
    public abstract class BaseRepository : IBaseRepository, IDisposable
    {
        private ApplicationDbContext _dataContext;

        protected internal ApplicationDbContext DataContext
        {
            get
            {
                if (_dataContext == null)
                {
                    _dataContext = AccessProvider.GetObjectContext();
                }

                return _dataContext;
            }
        }

        public BaseRepository()
        {
            _dataContext = AccessProvider.GetObjectContext();
        }

        public void Dispose()
        {
            if (_dataContext != null)
            {
                _dataContext.Dispose();
                _dataContext = null;
            }
        }
    }

    public abstract class BaseRepository<TBase> : IBaseRepository<TBase>, IDisposable where TBase : class
    {
        private ApplicationDbContext _dataContext;

        protected internal ApplicationDbContext DataContext
        {
            get
            {
                if (_dataContext == null)
                {
                    _dataContext = AccessProvider.GetObjectContext();
                }
                return _dataContext;
            }
        }

        public BaseRepository()
        {
            _dataContext = AccessProvider.GetObjectContext();
        }

        public void Dispose()
        {
            if (_dataContext != null)
            {
                _dataContext.Dispose();
                _dataContext = null;
            }
        }


        public TBase GetEntity(int id)
        {
            return DataContext.Set<TBase>().Find(id);
        }

        public TBase GetEntityBy(Expression<Func<TBase, bool>> predicate)
        {
            return DataContext.Set<TBase>().SingleOrDefault(predicate);
        }

        public TBase GetEntityBy(Expression<Func<TBase, bool>> predicate, params Expression<Func<TBase, object>>[] includeProperties)
        {
            foreach (var property in includeProperties)
            {
                DataContext.Set<TBase>().Include(property);
            }

            return DataContext.Set<TBase>().SingleOrDefault(predicate);
        }

        public virtual TBase AddEntity(TBase entity)
        {
            DataContext.Set<TBase>().Add(entity);
            DataContext.SaveChanges();
            return entity;
        }

        public virtual TBase UpdateEntity(TBase entity)
        {
            DataContext.Entry(entity).State = EntityState.Modified;
            DataContext.SaveChanges();
            return entity;
        }

        public virtual void DeleteEntity(TBase entity)
        {
            DataContext.Set<TBase>().Remove(entity);
            DataContext.SaveChanges();
        }


        protected IQueryable<TBase> GetAll()
        {
            return DataContext.Set<TBase>();
        }

        protected IQueryable<TBase> GetAllWith(params Expression<Func<TBase, object>>[] includeProperties)
        {
            foreach (var property in includeProperties)
            {
                DataContext.Set<TBase>().Include(property);
            }

            return DataContext.Set<TBase>();
        }

        protected IQueryable<TBase> GetElementsBy(Expression<Func<TBase, bool>> whereLambda)
        {
            return GetAll().Where(whereLambda);
        }


        public List<TBase> GetList()
        {
            return GetAll().ToList();
        }

        public List<TBase> GetList(params Expression<Func<TBase, object>>[] includeProperties)
        {
            return GetAllWith(includeProperties).ToList();
        }

        public List<TBase> GetListBy(Expression<Func<TBase, bool>> predicate)
        {
            return GetElementsBy(predicate).ToList();
        }

        public int GetCountListBy(Expression<Func<TBase, bool>> predicate)
        {
            return GetElementsBy(predicate).Count();
        }

        public List<TBase> GetListBy(Expression<Func<TBase, bool>> predicate, params Expression<Func<TBase, object>>[] includeProperties)
        {
            return GetAllWith(includeProperties).Where(predicate).ToList();
        }

        public List<TBase> GetPaginatedListBy<TKey>(Expression<Func<TBase, bool>> whereLambda, Expression<Func<TBase, TKey>> orderByLambda, int skipCount, int takeCount)
        {
            return GetAll().Where(whereLambda).OrderBy(orderByLambda).Skip(skipCount).Take(takeCount).ToList();
        }

        public List<TBase> GetPaginatedListBy<TKey>(Expression<Func<TBase, bool>> whereLambda, Expression<Func<TBase, TKey>> orderByLambda, int skipCount, int takeCount, params Expression<Func<TBase, object>>[] includeProperties)
        {
            return GetAllWith(includeProperties).Where(whereLambda).OrderBy(orderByLambda).Skip(skipCount).Take(takeCount).ToList();
        }

    }
}