﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mapy.Repository.Position
{
    public interface IPositionRepository : IBaseRepository<Mapy.Model.Entities.Position>
    {
    }
}
