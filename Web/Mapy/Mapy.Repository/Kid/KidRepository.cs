﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mapy.Repository.Kid
{
    public class KidRepository : BaseRepository<Mapy.Model.Entities.Kid>, IKidRepository
    {
        public Model.Entities.Kid GetByToken(string token)
        {
            return GetEntityBy(a => a.Token == token);
        }
    }
}
