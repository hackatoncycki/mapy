﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mapy.Repository.Kid
{
    public interface IKidRepository : IBaseRepository<Mapy.Model.Entities.Kid>
    {
        Mapy.Model.Entities.Kid GetByToken(string token);
    }
}
