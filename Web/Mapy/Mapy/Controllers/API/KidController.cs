﻿using Mapy.Repository.Kid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Mapy.API
{
    public class KidController : ApiController
    {
        internal readonly IKidRepository _kidRepository;
        
        public KidController(IKidRepository kidRepository)
        {
            _kidRepository = kidRepository;
        }

        [HttpGet]
        public int Get([FromUri]string token)
        {
            var kid = _kidRepository.GetByToken(token);

            return kid == null ? -1 : kid.Id;
        }
    }
}
