﻿using Mapy.Model.Entities;
using Mapy.Repository.Kid;
using Mapy.Repository.Position;
using Mapy.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Mapy.Controllers.API
{
    public class PositionController : ApiController
    {
        internal readonly IKidRepository _kidRepository;
        internal readonly IPositionRepository _positionRepository;

        public PositionController(IKidRepository kidRepository, IPositionRepository positionRepository)
        {
            _kidRepository = kidRepository;
            _positionRepository = positionRepository;
        }
        [HttpPost]
        public bool Post(NewPositionViewModel vModel)
        {
            var kid = _kidRepository.GetEntity(vModel.KidId);

            if(kid != null)
            {
                var entity = AutoMapper.Mapper.Map<NewPositionViewModel, Position>(vModel);
                entity.Kid = kid;
                _positionRepository.AddEntity(entity);
                return true;
            }

            return false;
        }
    }
}
