﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mapy.ViewModels
{
    public class NewPositionViewModel
    {
        public int KidId { get; set; }
        public float Longitude { get; set; }
        public float Latitude { get; set; }
        public DateTime Date { get; set; }
    }
}