﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Mapy.Startup))]
namespace Mapy
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);


        }
    }
}
