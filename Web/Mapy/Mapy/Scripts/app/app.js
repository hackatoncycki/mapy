﻿(function () {
    'use strict';

    config.$inject = ['$routeProvider', '$locationProvider'];

    angular.module('mapy', ['ngRoute']).config(config);

    function config($routeProvider, $locationProvider) {

        $routeProvider.          
            when('/', {
                templateUrl: 'Scripts/app/dashboard/dashboard.html',
                controller: 'kidsListCtrl'
            },null).
            otherwise({
                redirectTo: '/'
            });

            $locationProvider.html5Mode(false);
        }
})();