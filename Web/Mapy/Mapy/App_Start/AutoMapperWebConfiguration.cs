﻿using AutoMapper;
using Mapy.Model.Entities;
using Mapy.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mapy.App_Start
{
    public static class AutoMapperWebConfiguration
    {
        public static void Configure()
        {
            ConfigureUserMapping();
        }

        private static void ConfigureUserMapping()
        {
            Mapper.CreateMap<NewPositionViewModel, Position>();
        }

    }
}