[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Mapy.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(Mapy.App_Start.NinjectWebCommon), "Stop")]

namespace Mapy.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using Mapy.Repository.Kid;
    using Mapy.Repository.Target;
    using Mapy.Repository.Parent;
    using Mapy.Repository.Position;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                var httpResolver = new NinjectHttpDependencyResolver(kernel);

                System.Web.Http.GlobalConfiguration.Configuration.DependencyResolver = httpResolver;

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IKidRepository>().To<KidRepository>();
            kernel.Bind<ITargetRepository>().To<TargetRepository>();
            kernel.Bind<IParentRepository>().To<ParentRepository>();
            kernel.Bind<IPositionRepository>().To<PositionRepository>();
        }        
    }
}
