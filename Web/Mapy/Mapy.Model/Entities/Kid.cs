﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mapy.Model.Entities
{
    public class Kid
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public string Token { get; set; }

        public virtual ICollection<Position> Positions { get; set; }
        public virtual ICollection<Target> Targets { get; set; }
    }
}
