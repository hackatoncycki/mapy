namespace Mapy.Model.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Model;
    using System.Data.Entity.SqlServer;
    using System.Linq;

    public class AzureSqlGenerator : SqlServerMigrationSqlGenerator
    {
        protected override void Generate(CreateTableOperation createTableOperation)
        {
            if ((createTableOperation.PrimaryKey != null)
                && !createTableOperation.PrimaryKey.IsClustered)
            {
                createTableOperation.PrimaryKey.IsClustered = true;
            }

            base.Generate(createTableOperation);
        }
    }

    internal sealed class Configuration : DbMigrationsConfiguration<Mapy.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            SetSqlGenerator("System.Data.SqlClient", new AzureSqlGenerator());
        }

        protected override void Seed(Mapy.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
